package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.dbtools.schema.document.Column;
import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Oracle_ColumnType extends Function {

	public Oracle_ColumnType() {
		super(ExpressionType.String, "Oracle_ColumnType");
	}
	
	@Override
	public int getMinParameterCount() {
		return 1;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 1;
	}

	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}
	
	@Override
	public Object eval(Object... parameters) {
		Column column = (Column) parameters[0];
		switch (column.getType()) {
		case Bit					: 
		case Boolean				: return "NUMBER(1,0)";

		case TinyInt				: 
		case SmallInt				: 
		case Integer				: return "NUMBER(10,0)";
		
		case BigInt					: return "NUMBER(19,0)";

		case Float					: 
		case Real					: 
		case Double					: return "NUMBER";
		case Numeric				: 
		case Decimal				: return "NUMBER(" + column.getLength() + "," + column.getLength2() + ")";

		case Char					: return "CHAR(" + column.getLength() + ")";
		case Varchar				: return "VARCHAR2(" + column.getLength() + ")";
		case LongVarchar			: return "LONG";
		case Clob					: return "CLOB";
		case RowId					: return "ROWID";
		case NChar					: return "NCHAR(" + column.getLength() + ")";
		case NVarChar				: return "NVARCHAR2(" + column.getLength() + ")";
		case LongNVarChar			: return "NCLOB";
		case NClob					: return "NCLOB";
		
		case Date					: 
		case Time					: 
		case Timestamp				: 
		case TimeWithTimeZone		: 
		case TimestampWithTimeZone	: return "DATE";
		
		case Binary					: 
		case VarBinary				: 
		case LongVarBinary			: 
		case Blob					: return "BLOB";

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return "UNKNOWN";
		}
	}
}
