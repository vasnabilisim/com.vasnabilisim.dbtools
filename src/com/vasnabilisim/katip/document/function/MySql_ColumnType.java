package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.dbtools.schema.document.Column;
import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class MySql_ColumnType extends Function {

	public MySql_ColumnType() {
		super(ExpressionType.String, "MySql_ColumnType");
	}
	
	@Override
	public int getMinParameterCount() {
		return 1;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 1;
	}

	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}
	
	@Override
	public Object eval(Object... parameters) {
		Column column = (Column) parameters[0];
		switch (column.getType()) {
		case Bit					: return "BIT(" + (column.getLength() <= 1 ? 1 : column.getLength()) + ")";
		case Boolean				: return "BOOLEAN";

		case TinyInt				: return "TINYINT";
		case SmallInt				: return "SMALLINT";
		case Integer				: return "INTEGER";
		
		case BigInt					: return "BIGINT";

		case Float					: return "FLOAT";
		case Real					: 
		case Double					: return "DOUBLE";
		case Numeric				: 
		case Decimal				: return "DECIMAL(" + column.getLength() + "," + column.getLength2() + ")";

		case Char					: return "CHAR(" + column.getLength() + ")";
		case Varchar				: return "VARCHAR(" + column.getLength() + ")";
		case LongVarchar			: return "VARCHAR(" + column.getLength() + ")";
		case Clob					: return "TEXT";
		case RowId					: return "CHAR(36)";
		case NChar					: return "CHAR(" + column.getLength() + ")";
		case NVarChar				: return "VARCHAR(" + column.getLength() + ")";
		case LongNVarChar			: return "VARCHAR(" + column.getLength() + ")";
		case NClob					: return "TEXT";
		
		case Date					: return "DATE";
		case Time					: return "TIME";
		case Timestamp				: return "DATETIME";
		case TimeWithTimeZone		: return "TIME";
		case TimestampWithTimeZone	: return "DATETIME";
		
		case Binary					: return "BINARY(" + column.getLength() + ")";
		case VarBinary				: return "VARBINARY(" + column.getLength() + ")";
		case LongVarBinary			: return "VARBINARY(" + column.getLength() + ")";
		case Blob					: return "BLOB";

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return "UNKNOWN";
		}
	}
}
