package com.vasnabilisim.katip.document.function;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import com.vasnabilisim.dbtools.schema.document.Column;
import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Oracle_ColumnValue extends Function {

	public Oracle_ColumnValue() {
		super(ExpressionType.String, "Oracle_ColumnValue");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 2;
	}

	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}
	
	@Override
	public Object eval(Object... parameters) {
		Column column = (Column) parameters[0];
		Object value = parameters[1];
		if(value == null)
			return "NULL";
		switch (column.getType()) {
		case Bit					: 
		case Boolean				: return (Boolean)value ? "1" : "0";

		case TinyInt				: 
		case SmallInt				: 
		case Integer				: 
		case BigInt					: 
		case Float					: 
		case Real					: 
		case Double					: 
		case Numeric				: 
		case Decimal				: return value.toString();

		case Char					: 
		case Varchar				: 
		case LongVarchar			: 
		case Clob					: 
		case RowId					: 
		case NChar					: 
		case NVarChar				: 
		case LongNVarChar			: 
		case NClob					: return "'" + encode(value.toString()) + "'";
		
		case Date					: return "TO_DATE('" + ((LocalDate)value).format(Formatter_Date) + "', 'YYYY-MM-DD')";
		case Time					: return "TO_DATE('" + ((LocalTime)value).format(Formatter_Time) + "', 'HH24-MI-SS')";
		case Timestamp				: return "TO_DATE('" + ((LocalDateTime)value).format(Formatter_DateTime) + "', 'YYYY-MM-DD-HH24-MI-SS')";
		case TimeWithTimeZone		: return "TO_DATE('" + Format_Time.format((java.sql.Time)value) + "', 'HH24-MI-SS')";
		case TimestampWithTimeZone	: return "TO_DATE('" + Format_DateTime.format((java.sql.Time)value) + "', 'YYYY-MM-DD-HH24-MI-SS')";
		
		case Binary					: 
		case VarBinary				: 
		case LongVarBinary			: 
		case Blob					: return "NULL";

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return "NULL";
		}
	}
	
	static Pattern pattern = Pattern.compile("'");
	static String encode(String s) {
		return pattern.matcher(s).replaceAll("''");
	}

	static DateTimeFormatter Formatter_DateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
	static DateTimeFormatter Formatter_Date = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	static DateTimeFormatter Formatter_Time = DateTimeFormatter.ofPattern("HH-mm-ss");
	static SimpleDateFormat Format_DateTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	static SimpleDateFormat Format_Time = new SimpleDateFormat("HH-mm-ss");
}
