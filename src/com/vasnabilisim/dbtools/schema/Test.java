package com.vasnabilisim.dbtools.schema;

import com.vasnabilisim.dbtools.schema.document.Schema;
import com.vasnabilisim.dbtools.schema.document.SchemaIO;
import com.vasnabilisim.xml.XmlException;

public class Test {

	public Test() {
	}

	public static void main(String[] args) {
		
		Schema schema;
		try {
			schema = SchemaIO.read("db-schema/sakila.xml");
			SchemaIO.write(schema, "db-schema/sakila2.xml");
		} catch (XmlException e) {
			e.printStackTrace();
			return;
		}
	}
}
