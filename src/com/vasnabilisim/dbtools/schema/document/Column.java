package com.vasnabilisim.dbtools.schema.document;

/**
 * @author Menderes Fatih G�VEN
 */
public class Column {

	Table table;
	String name;
	ColumnType type;
	Integer length;
	Integer length2;
	Boolean primary = Boolean.FALSE;
	Boolean notNull = Boolean.FALSE;
	String information;
	
	public Column() {
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ColumnType getType() {
		return type;
	}
	
	public void setType(ColumnType type) {
		this.type = type;
	}
	
	public Integer getLength() {
		return length;
	}
	
	public void setLength(Integer length) {
		this.length = length;
	}
	
	public Integer getLength2() {
		return length2;
	}
	
	public void setLength2(Integer length2) {
		this.length2 = length2;
	}
	
	public Boolean getPrimary() {
		return primary;
	}
	
	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}
	
	public Boolean getNotNull() {
		return notNull;
	}
	
	public void setNotNull(Boolean notNull) {
		this.notNull = notNull;
	}
	
	public String getInformation() {
		return information;
	}
	
	public void setInformation(String information) {
		this.information = information;
	}
}
