package com.vasnabilisim.dbtools.schema.document;

import java.math.BigDecimal;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

/**
 * @author Menderes Fatih G�VEN
 */
public enum ColumnType {

	Bit(ColumnTypeGroup.Boolean, Types.BIT), 
	TinyInt(ColumnTypeGroup.Integer, Types.TINYINT),
	SmallInt(ColumnTypeGroup.Integer, Types.SMALLINT),
	Integer(ColumnTypeGroup.Integer, Types.INTEGER),
	BigInt(ColumnTypeGroup.Integer, Types.BIGINT),
	Float(ColumnTypeGroup.Decimal, Types.FLOAT),
	Real(ColumnTypeGroup.Decimal, Types.REAL),
	Double(ColumnTypeGroup.Decimal, Types.DOUBLE),
	Numeric(ColumnTypeGroup.Decimal, Types.NUMERIC),
	Decimal(ColumnTypeGroup.Decimal, Types.DECIMAL),
	Char(ColumnTypeGroup.String, Types.CHAR),
	Varchar(ColumnTypeGroup.String, Types.VARCHAR),
	LongVarchar(ColumnTypeGroup.String, Types.LONGVARCHAR),
	Date(ColumnTypeGroup.Date, Types.DATE),
	Time(ColumnTypeGroup.Time, Types.TIME),
	Timestamp(ColumnTypeGroup.DateTime, Types.TIMESTAMP),
	Binary(ColumnTypeGroup.Binary, Types.BINARY),
	VarBinary(ColumnTypeGroup.Binary, Types.VARBINARY),
	LongVarBinary(ColumnTypeGroup.Binary, Types.LONGVARBINARY),
	Null(ColumnTypeGroup.Other, Types.NULL),
	Other(ColumnTypeGroup.Other, Types.OTHER),
	JavaObject(ColumnTypeGroup.Other, Types.JAVA_OBJECT),
	Distinct(ColumnTypeGroup.Other, Types.DISTINCT),
	Struct(ColumnTypeGroup.Other, Types.STRUCT),
	Array(ColumnTypeGroup.Other, Types.ARRAY),
	Blob(ColumnTypeGroup.Binary, Types.BLOB),
	Clob(ColumnTypeGroup.String, Types.CLOB),
	Ref(ColumnTypeGroup.Other, Types.REF),
	DataLink(ColumnTypeGroup.Other, Types.DATALINK),
	Boolean(ColumnTypeGroup.Boolean, Types.BOOLEAN),
	RowId(ColumnTypeGroup.String, Types.ROWID),
	NChar(ColumnTypeGroup.String, Types.NCHAR),
	NVarChar(ColumnTypeGroup.String, Types.NVARCHAR),
	LongNVarChar(ColumnTypeGroup.String, Types.LONGNVARCHAR),
	NClob(ColumnTypeGroup.String, Types.NCLOB),
	SqlXml(ColumnTypeGroup.Other, Types.SQLXML),
	RefCursor(ColumnTypeGroup.Other, Types.REF_CURSOR),
	TimeWithTimeZone(ColumnTypeGroup.Time, Types.TIME_WITH_TIMEZONE),
	TimestampWithTimeZone(ColumnTypeGroup.DateTime, Types.TIMESTAMP_WITH_TIMEZONE);
	

	ColumnTypeGroup group;
	int sqlType;
	
	ColumnType(ColumnTypeGroup group, int sqlType) {
		this.group = group;
		this.sqlType = sqlType;
	}
	
	public ColumnTypeGroup getGroup() {
		return group;
	}
	
	public int getSqlType() {
		return sqlType;
	}
	
	public String toString(Object value) {
		switch (this) {
		case Bit					: 
		case Boolean				: 
		case TinyInt				: 
		case SmallInt				: 
		case Integer				: 
		case BigInt					: 
		case Float					: 
		case Real					: 
		case Double					: 
		case Numeric				: 
		case Decimal				: 
		case Char					: 
		case Varchar				: 
		case LongVarchar			: 
		case Clob					: 
		case RowId					: 
		case NChar					: 
		case NVarChar				: 
		case LongNVarChar			: 
		case NClob					: return value.toString();
		
		case Date					: return Formatter_Date.format((LocalDate)value);
		case Time					: return Formatter_Time.format((LocalTime)value);
		case Timestamp				: return Formatter_DateTime.format((LocalDateTime)value);
		case TimeWithTimeZone		: return Format_Time.format((java.sql.Time)value);
		case TimestampWithTimeZone	: return Format_DateTime.format((java.sql.Timestamp)value);
		
		case Binary					: 
		case VarBinary				: 
		case LongVarBinary			: 
		case Blob					: return Base64.getUrlEncoder().encodeToString((byte[])value);

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return null;
		}
	}
	
	public Object fromString(String s) throws Exception {
		switch (this) {
		case Bit					: 
		case Boolean				: return java.lang.Boolean.valueOf(s);

		case TinyInt				: 
		case SmallInt				: 
		case Integer				: return java.lang.Integer.valueOf(s);
		
		case BigInt					: return java.lang.Long.valueOf(s);

		case Float					: 
		case Real					: 
		case Double					: 
		case Numeric				: 
		case Decimal				: return new BigDecimal(s);
		
		case Char					: 
		case Varchar				: 
		case LongVarchar			: 
		case Clob					: 
		case RowId					: 
		case NChar					: 
		case NVarChar				: 
		case LongNVarChar			: 
		case NClob					: return s;
		
		case Date					: return LocalDate.parse(s, Formatter_Date);
		case Time					: return LocalTime.parse(s, Formatter_Time);
		case Timestamp				: return LocalDateTime.parse(s, Formatter_DateTime);
		case TimeWithTimeZone		: return new java.sql.Time(Format_Time.parse(s).getTime());
		case TimestampWithTimeZone	: return new java.sql.Timestamp(Format_DateTime.parse(s).getTime());
		
		case Binary					: 
		case VarBinary				: 
		case LongVarBinary			: 
		case Blob					: return Base64.getUrlDecoder().decode(s);

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return null;
		}
	}

	static DateTimeFormatter Formatter_DateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
	static DateTimeFormatter Formatter_Date = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	static DateTimeFormatter Formatter_Time = DateTimeFormatter.ofPattern("HH-mm-ss");
	static SimpleDateFormat Format_DateTime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	static SimpleDateFormat Format_Time = new SimpleDateFormat("HH-mm-ss");
	
	/**
	 * Returns the corresponding type of given sql type.
	 * @param type
	 * @return
	 */
	public static ColumnType fromSqlType(int type) {
		for(ColumnType columnType : values()) {
			if(columnType.sqlType == type)
				return columnType;
		}
		return Null;
	}
}
