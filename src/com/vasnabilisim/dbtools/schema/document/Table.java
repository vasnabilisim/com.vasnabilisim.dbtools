package com.vasnabilisim.dbtools.schema.document;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Menderes Fatih G�VEN
 */
public class Table {

	Schema schema;
	String name;
	
	ArrayList<Column> columns = new ArrayList<>();
	ArrayList<Column> primaryColumns = new ArrayList<>();
	
	ArrayList<Index> indexes = new ArrayList<Index>();
	
	ArrayList<ForeignKey> foreignKeys = new ArrayList<ForeignKey>();
	ArrayList<ForeignKey> referencedForeignKeys = new ArrayList<ForeignKey>();
	
	List<List<Object>> rows = new LinkedList<>();
	
	public Table() {
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public Schema getSchema() {
		return schema;
	}
	
	public void setSchema(Schema schema) {
		this.schema = schema;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getColumnCount() {
		return columns.size();
	}
	
	public List<Column> getColumns() {
		return columns;
	}
	
	public Column getColumn(String name) {
		for(Column column : columns)
			if(name.equals(column.getName()))
				return column;
		return null;
	}
	
	public void addColumn(Column column) {
		column.setTable(this);
		columns.add(column);
		if(column.getPrimary()) 
			primaryColumns.add(column);
	}
	
	public List<Column> getPrimaryColumns() {
		return primaryColumns;
	}
	
	public List<Index> getIndexes() {
		return indexes;
	}
	
	public void addIndex(Index index) {
		index.setTable(this);
		indexes.add(index);
	}
	
	public ArrayList<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}
	
	public void addForeignKey(ForeignKey foreignKey) {
		foreignKey.setTable(this);
		foreignKeys.add(foreignKey);
	}
	
	public ArrayList<ForeignKey> getReferencedForeignKeys() {
		return referencedForeignKeys;
	}
	
	public void addReferencedForeignKey(ForeignKey foreignKey) {
		foreignKey.setReferencedTable(this);
		referencedForeignKeys.add(foreignKey);
	}
	
	public int getRowCount() {
		return rows.size();
	}
	
	public List<List<Object>> getRows() {
		return rows;
	}
	
	public List<Object> getRow(int rowIndex) {
		return rows.get(rowIndex);
	}
	
	public Object getCellValue(int rowIndex, int colIndex) {
		return rows.get(rowIndex).get(colIndex);
	}
	
	public void addRow(List<Object> row) {
		rows.add(row);
	}
}
