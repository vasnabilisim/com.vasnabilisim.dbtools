package com.vasnabilisim.dbtools.schema.document;

import java.util.ArrayList;

/**
 * @author Menderes Fatih G�VEN
 */
public class ForeignKey {

	String name;
	Table table;
	Table referencedTable;
	ArrayList<ForeignKeyColumn> foreignKeyColumns = new ArrayList<ForeignKeyColumn>();
	
	public ForeignKey() {
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}
	
	public Table getReferencedTable() {
		return referencedTable;
	}
	
	public void setReferencedTable(Table referencedTable) {
		this.referencedTable = referencedTable;
	}

	public ArrayList<ForeignKeyColumn> getForeignKeyColumns() {
		return foreignKeyColumns;
	}
	
	public void addForeignKeyColumn(ForeignKeyColumn foreignKeyColumn) {
		foreignKeyColumn.setForeignKey(this);
		foreignKeyColumns.add(foreignKeyColumn);
	}
}
