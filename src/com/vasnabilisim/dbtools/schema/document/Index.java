package com.vasnabilisim.dbtools.schema.document;

import java.util.ArrayList;

/**
 * @author Menderes Fatih G�VEN
 */
public class Index {

	Table table;
	String name;
	Boolean unique = Boolean.FALSE;
	ArrayList<IndexColumn> indexColumns = new ArrayList<IndexColumn>();
	
	public Index() {
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getUnique() {
		return unique;
	}
	
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
	
	public ArrayList<IndexColumn> getIndexColumns() {
		return indexColumns;
	}
	
	public void addIndexColumn(IndexColumn indexColumn) {
		indexColumn.setIndex(this);
		indexColumns.add(indexColumn);
	}
}
