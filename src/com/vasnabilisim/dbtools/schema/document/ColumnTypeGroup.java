package com.vasnabilisim.dbtools.schema.document;

/**
 * @author Menderes Fatih G�VEN
 */
public enum ColumnTypeGroup {

	String, 
	Integer, 
	Decimal, 
	DateTime, 
	Date, 
	Time, 
	Boolean, 
	Binary, 
	Other;
}
