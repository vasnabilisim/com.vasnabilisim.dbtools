package com.vasnabilisim.dbtools.schema.document;

/**
 * @author Menderes Fatih G�VEN
 */
public enum IndexColumnOrder {

	asc, 
	desc
}
