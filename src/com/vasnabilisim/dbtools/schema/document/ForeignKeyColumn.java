package com.vasnabilisim.dbtools.schema.document;


/**
 * @author Menderes Fatih G�VEN
 */
public class ForeignKeyColumn {

	ForeignKey foreignKey;
	Column column;
	Column referencedColumn;
	
	public ForeignKeyColumn() {
	}
	
	@Override
	public String toString() {
		return column.getName() + "-" + referencedColumn.getName();
	}

	public ForeignKey getForeignKey() {
		return foreignKey;
	}
	
	public void setForeignKey(ForeignKey foreignKey) {
		this.foreignKey = foreignKey;
	}
	
	public Column getColumn() {
		return column;
	}
	
	public void setColumn(Column column) {
		this.column = column;
	}
	
	public Column getReferencedColumn() {
		return referencedColumn;
	}
	
	public void setReferencedColumn(Column referencedColumn) {
		this.referencedColumn = referencedColumn;
	}
}
