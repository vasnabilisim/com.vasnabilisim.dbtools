package com.vasnabilisim.dbtools.schema.document;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.vasnabilisim.jdbc.DataSourceInfo;

/**
 * @author Menderes Fatih G�VEN
 */
public final class SchemaJdbc {

	public static Schema read(DataSourceInfo dataSourceInfo, String schemaName, String tableNames, String rowNames) throws SQLException {
		SchemaJdbc schemaJdbc = new SchemaJdbc(dataSourceInfo, schemaName, tableNames, rowNames);
		return schemaJdbc.read();
	}

	DataSourceInfo dataSourceInfo; 
	String schemaName; 
	String tableNames; 
	String rowNames;

	private SchemaJdbc(
			DataSourceInfo dataSourceInfo, 
			String schemaName, 
			String tableNames, 
			String rowNames) 
	{
		this.dataSourceInfo = dataSourceInfo;
		this.schemaName = schemaName;
		this.tableNames = tableNames;
		this.rowNames = rowNames;
	}

	public static boolean LogEnabled = false;
	static void log(String text) {
		if(LogEnabled)
			System.out.println(text);
			//Logger.info(text);
	}

	Schema read() throws SQLException {
		log(String.format("Connecting %s", dataSourceInfo.getDataSourceUrl()));
		Connection connection = dataSourceInfo.createConnection();
		log("Connection Succesful");
		
		log("Reading schema");
		DatabaseMetaData metaData = connection.getMetaData();
		Schema schema = new Schema();
		schema.setName(schemaName);
		
		readTables(metaData, schema, tableNames);
		readColumns(metaData, schema);
		readIndexes(metaData, schema);
		readForeignKeys(metaData, schema);
		
		readRows(metaData, schema, rowNames);
		
		connection.close();
		log("Reading schema Done");
		return schema;
	}

	void readTables(DatabaseMetaData metaData, Schema schema, String tableNames) throws SQLException {
		List<String> tableNameList = null;
		if(tableNames == null) {
			tableNameList = new ArrayList<String>(1);
			tableNameList.add(null);
		}
		else {
			tableNameList = Arrays.asList(tableNames.split(";"));
		}
		for(String seachTableName : tableNameList) {
			log(String.format("Reading table(s) %s", seachTableName == null ? "" : seachTableName));
			ResultSet tableResultSet = metaData.getTables(null, schema.getName(), seachTableName, new String[] {"TABLE"});
			while(tableResultSet.next()) {
				String tableName = tableResultSet.getString("TABLE_NAME");
				if(schema.getTable(tableName) != null)
					continue;
				Table table = new Table();
				table.setName(tableName);
				schema.addTable(table);
			}
			tableResultSet.close();
		}
	}
	
	void readColumns(DatabaseMetaData metaData, Schema schema) throws SQLException {
		for(Table table : schema.getTables()) {
			log(String.format("Reading columns of table %s", table.getName()));
			Set<String> primaryColumns = new TreeSet<>();
			ResultSet primaryKeyResultSet = metaData.getPrimaryKeys(null, schema.getName(), table.getName());
			while(primaryKeyResultSet.next()) {
				primaryColumns.add(primaryKeyResultSet.getString("COLUMN_NAME"));
			}
			primaryKeyResultSet.close();
			
			ResultSet columnResultSet = metaData.getColumns(null, schema.getName(), table.getName(), null);
			while(columnResultSet.next()) {
				String columnName = columnResultSet.getString("COLUMN_NAME");
				ColumnType columnType = ColumnType.fromSqlType(columnResultSet.getInt("DATA_TYPE"));
				Boolean columnNotNull = columnResultSet.getInt("NULLABLE") == 0;
				Integer columnLength = columnResultSet.getInt("COLUMN_SIZE");
				Integer columnLength2 = columnResultSet.getInt("DECIMAL_DIGITS");
				Column column = new Column();
				column.setName(columnName);
				column.setType(columnType);
				column.setLength(columnLength);
				column.setLength2(columnLength2);
				column.setPrimary(primaryColumns.contains(columnName));
				column.setNotNull(columnNotNull);
				table.addColumn(column);
			}
			columnResultSet.close();
		}
	}

	void readIndexes(DatabaseMetaData metaData, Schema schema) throws SQLException {
		for(Table table : schema.getTables()) {
			log(String.format("Reading indexes of table %s", table.getName()));
			ResultSet indexResultSet = metaData.getIndexInfo(null, schema.getName(), table.getName(), false, true);
			Index index = new Index();
			while(indexResultSet.next()) {
				String indexName = indexResultSet.getString("INDEX_NAME");
				if(indexName == null)
					continue;
				if(!indexName.equals(index.getName())) {
					index = new Index();
					index.setName(indexName);
					index.setUnique(!indexResultSet.getBoolean("NON_UNIQUE"));
					table.addIndex(index);
				}
				IndexColumn indexColumn = new IndexColumn();
				indexColumn.setColumn(table.getColumn(indexResultSet.getString("COLUMN_NAME")));
				indexColumn.setOrder("A".equals(indexResultSet.getString("ASC_OR_DESC")) ? IndexColumnOrder.asc : IndexColumnOrder.desc);
				index.addIndexColumn(indexColumn);
			}
			indexResultSet.close();
		}
	}

	void readForeignKeys(DatabaseMetaData metaData, Schema schema) throws SQLException {
		for(Table table : schema.getTables()) {
			log(String.format("Reading foreign keys of table %s", table.getName()));
			ResultSet foreignKeyResultSet = metaData.getImportedKeys(null, schema.getName(), table.getName());
			ForeignKey foreignKey = new ForeignKey();
			while(foreignKeyResultSet.next()) {
				String foreignKeyName = foreignKeyResultSet.getString("FK_NAME");
				if(!foreignKeyName.equals(foreignKey.getName())) {
					foreignKey = new ForeignKey();
					foreignKey.setName(foreignKeyName);
					Table referencedTable = schema.getTable(foreignKeyResultSet.getString("PKTABLE_NAME"));
					if(referencedTable == null)
						continue;
					foreignKey.setReferencedTable(referencedTable);
					table.addForeignKey(foreignKey);
					referencedTable.addReferencedForeignKey(foreignKey);
				}
				ForeignKeyColumn foreignKeyColumn = new ForeignKeyColumn();
				String fkColumnName = foreignKeyResultSet.getString("FKCOLUMN_NAME");
				String pkColumnName = foreignKeyResultSet.getString("PKCOLUMN_NAME");
				foreignKeyColumn.setColumn(table.getColumn(fkColumnName));
				foreignKeyColumn.setReferencedColumn(foreignKey.getReferencedTable().getColumn(pkColumnName));
				
				foreignKey.addForeignKeyColumn(foreignKeyColumn);
			}
			foreignKeyResultSet.close();
		}
	}

	void readRows(DatabaseMetaData metaData, Schema schema, String tableNames) throws SQLException {
		if(tableNames == null)
			return;
		log("Reading rows");
		List<String> tableNameList = Arrays.asList(tableNames.split(";"));
		List<Table> tableList = new ArrayList<>();
		for(String seachTableName : tableNameList) {
			ResultSet tableResultSet = metaData.getTables(null, schema.getName(), seachTableName, new String[] {"TABLE"});
			while(tableResultSet.next()) {
				String tableName = tableResultSet.getString("TABLE_NAME");
				Table table = schema.getTable(tableName);
				if(table == null)
					log(String.format("Reading rows of table % skiped", tableName));
				else
					tableList.add(table);
			}
			tableResultSet.close();
		}
		
		Connection connection = dataSourceInfo.createConnection();
		for(Table table : tableList) {
			log(String.format("Reading rows of table %s", table.getName()));
			String sql = String.format("SELECT * FROM %s.%s", schema.getName(), table.getName());
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				int columnCount = table.getColumnCount();
				ArrayList<Object> row = new ArrayList<Object>(columnCount);
				int columnIndex = 0;
				for(Column column : table.getColumns()) {
					++columnIndex;
					row.add(get(resultSet, columnIndex, column.getType()));
				}
				table.addRow(row);
			}
			log(String.format("%d rows read from table %s", table.getRowCount(), table.getName()));
			resultSet.close();
			statement.close();
		}
		connection.close();
	}
	
	static Object get(ResultSet resultSet, int index, ColumnType type) throws SQLException {
		switch (type) {
		case Bit					: 
		case Boolean				: return resultSet.getBoolean(index);

		case TinyInt				: 
		case SmallInt				: 
		case Integer				: return resultSet.getInt(index);
		
		case BigInt					: return resultSet.getLong(index);

		case Float					: 
		case Real					: 
		case Double					: 
		case Numeric				: 
		case Decimal				: return resultSet.getBigDecimal(index);

		case Char					: 
		case Varchar				: 
		case LongVarchar			: 
		case RowId					: 
		case NChar					: 
		case NVarChar				: 
		case LongNVarChar			: return resultSet.getString(index);

		case Clob					: 
		case NClob					: return resultSet.getClob(index);
		
		case Date					: return toLocalDate(resultSet.getDate(index));
		case Time					: return toLocalTime(resultSet.getTime(index));
		case Timestamp				: return toLocalDateTime(resultSet.getTimestamp(index));
		case TimeWithTimeZone		: return resultSet.getTime(index);
		case TimestampWithTimeZone	: return resultSet.getTimestamp(index);
		
		case Binary					: 
		case VarBinary				: 
		case LongVarBinary			: return resultSet.getBytes(index);
		
		case Blob					: return resultSet.getBlob(index);

		case Null					:
		case Other					:
		case JavaObject				:
		case Distinct				:
		case Struct					:
		case Array					:
		case Ref					:
		case DataLink				:
		case SqlXml					:
		case RefCursor				:
		default						: return null;
		}
	}
	
	static LocalDateTime toLocalDateTime(java.sql.Timestamp datetime) {
		return datetime == null ? null : datetime.toLocalDateTime();
	}
	
	static LocalDate toLocalDate(java.sql.Date date) {
		return date == null ? null : date.toLocalDate();
	}
	
	static LocalTime toLocalTime(java.sql.Time time) {
		return time == null ? null : time.toLocalTime();
	}
}
