package com.vasnabilisim.dbtools.schema.document;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.util.Pair;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xml.XmlParser;

public final class SchemaIO extends XmlParser {

	private SchemaIO() {
	}

	public static Schema read(String fileName) throws XmlException {
		return read(fromFileToDocument(new File(fileName)));
	}
	
	public static Schema read(File file) throws XmlException {
		return read(fromFileToDocument(file));
	}
	
	public static Schema read(InputStream in) throws XmlException {
		return read(fromInputStreamToDocument(in));
	}
	
	public static Schema read(Reader reader) throws XmlException {
		return read(fromReaderToDocument(reader));
	}
	
	public static Schema read(Document document) throws XmlException {
		Element schemaElement = document.getDocumentElement();
		if(!"schema".equals(schemaElement.getNodeName()))
			throw new XmlException(String.format("Unexpected root. Expecting schema, found [%s].", schemaElement.getNodeName()));
		String schemaName = getProperty(schemaElement, "name", true);
		Schema schema = new Schema();
		schema.setName(schemaName);
		
		TreeMap<String, Pair<Table, TreeMap<String, Column>>> tableMap = new TreeMap<>();
		List<Element> tableElements = findChildElements(schemaElement, "table");
		for(Element tableElement : tableElements) {
			String tableName = getStringProperty(tableElement, "name", true);
			log(String.format("Reading table %s", tableName));
			Table table = new Table();
			table.setName(tableName);
			
			TreeMap<String, Column> columnMap = new TreeMap<>();
			List<Element> columnElements = findChildElements(tableElement, "column");
			for(Element columnElement : columnElements) {
				String columnName = getStringProperty(columnElement, "name", true);
				ColumnType columnType = getEnumProperty(columnElement, "type", true, ColumnType.class);
				Integer columnLength = getIntegerProperty(columnElement, "length", false);
				Integer columnLength2 = getIntegerProperty(columnElement, "length", false);
				Boolean columnPrimary = getBooleanProperty(columnElement, "primary", false);
				Boolean columnNotNull = getBooleanProperty(columnElement, "not-null", false);
				String columnInformation = getStringProperty(columnElement, "information", false);
				
				Column column = new Column();
				column.setName(columnName);
				column.setType(columnType);
				if(columnLength != null)
					column.setLength(columnLength);
				if(columnLength2 != null)
					column.setLength2(columnLength2);
				if(columnPrimary != null)
					column.setPrimary(columnPrimary);
				if(columnNotNull != null)
					column.setNotNull(columnNotNull);
				if(columnInformation != null)
					column.setInformation(columnInformation);
				
				table.addColumn(column);
				columnMap.put(columnName, column);
			}
			schema.addTable(table);
			tableMap.put(tableName, new Pair<Table, TreeMap<String, Column>>(table, columnMap));

			List<Element> indexElements = findChildElements(tableElement, "index");
			for(Element indexElement : indexElements) {
				String indexName = getStringProperty(indexElement, "name", true);
				Boolean indexUnique = getBooleanProperty(indexElement, "unique", false);
				
				Index index = new Index();
				index.setName(indexName);
				if(indexUnique != null)
					index.setUnique(indexUnique);
				table.addIndex(index);

				List<Element> indexColumnElements = findChildElements(indexElement, "index-column");
				for(Element indexColumnElement : indexColumnElements) {
					String indexColumnName = getStringProperty(indexColumnElement, "column", true);
					Column column = columnMap.get(indexColumnName);
					if(column == null)
						throw new XmlException(String.format("Unknown column [%s].", indexColumnName));
					IndexColumnOrder indexColumnOrder = getEnumProperty(indexColumnElement, "order", true, IndexColumnOrder.class);
					IndexColumn indexColumn = new IndexColumn();
					indexColumn.setColumn(column);
					indexColumn.setOrder(indexColumnOrder);
					index.addIndexColumn(indexColumn);
				}
			}

		}
		
		for(Element tableElement : tableElements) {
			String tableName = getStringProperty(tableElement, "name", true);
			Pair<Table, TreeMap<String, Column>> tablePair = tableMap.get(tableName);
			List<Element> foreignKeyElements = findChildElements(tableElement, "foreign-key");
			for(Element foreignKeyElement : foreignKeyElements) {
				String foreignKeyName = getStringProperty(foreignKeyElement, "name", true);
				String foreignKeyReferencedTable = getStringProperty(foreignKeyElement, "referenced-table", true);
				Pair<Table, TreeMap<String, Column>> referencedTablePair = tableMap.get(foreignKeyReferencedTable);
				if(referencedTablePair == null)
					throw new XmlException(String.format("Unknown table [%s].", foreignKeyReferencedTable));
				ForeignKey foreignKey = new ForeignKey();
				foreignKey.setName(foreignKeyName);
				foreignKey.setReferencedTable(referencedTablePair.getFirst());
				
				List<Element> foreignKeyColumnElements = findChildElements(foreignKeyElement, "foreign-key-column");
				for(Element foreignKeyColumnElement : foreignKeyColumnElements) {
					String foreignKeyColumnColumn = getStringProperty(foreignKeyColumnElement, "column", true);
					String foreignKeyColumnReferencedColumn = getStringProperty(foreignKeyColumnElement, "referenced-column", true);
					Column column = tablePair.getSecond().get(foreignKeyColumnColumn);
					if(column == null)
						throw new XmlException(String.format("Unknown column [%s].", foreignKeyColumnColumn));
					Column referencedColumn = referencedTablePair.getSecond().get(foreignKeyColumnReferencedColumn);
					if(referencedColumn == null)
						throw new XmlException(String.format("Unknown column [%s].", foreignKeyColumnReferencedColumn));
				
					ForeignKeyColumn foreignKeyColumn = new ForeignKeyColumn();
					foreignKeyColumn.setColumn(column);
					foreignKeyColumn.setReferencedColumn(referencedColumn);
					
					foreignKey.addForeignKeyColumn(foreignKeyColumn);
				}
				
				tablePair.getFirst().addForeignKey(foreignKey);
				referencedTablePair.getFirst().addReferencedForeignKey(foreignKey);
			}
		}

		for(Element tableElement : tableElements) {
			String tableName = getStringProperty(tableElement, "name", true);
			Table table = tableMap.get(tableName).getFirst();
			List<Element> rowElements = findChildElements(tableElement, "row");
			if(!rowElements.isEmpty()) 
				log(String.format("Reading rows of %s", tableName));
			for(Element rowElement : rowElements) {
				List<Object> row = fromRowString(table.getColumns(), rowElement.getTextContent());
				table.addRow(row);
			}
		}
		
		return schema;
	}
	
	public static <E extends Enum<E>> E getEnumProperty(Element element, String propertyName, boolean must, Class<E> enumType) throws XmlException {
		String stringValue = getProperty(element, propertyName, must);
		try {
			return Enum.valueOf(enumType, stringValue);
		} catch (Exception e) {
			throw new XmlException(String.format("Unknown value [%s].", stringValue));
		}
	}
	
	public static Integer getIntegerProperty(Element element, String propertyName, boolean must) throws XmlException {
		String stringValue = getProperty(element, propertyName, must);
		if(stringValue == null)
			return null;
		try {
			return Integer.valueOf(stringValue);
		} catch (NumberFormatException e) {
			throw new XmlException(String.format("Unexpected integer value [%s].", stringValue));
		}
	}
	
	public static Boolean getBooleanProperty(Element element, String propertyName, boolean must) throws XmlException {
		String stringValue = getProperty(element, propertyName, must);
		return Boolean.valueOf(stringValue);
	}
	
	public static String getStringProperty(Element element, String propertyName, boolean must) throws XmlException {
		return getProperty(element, propertyName, must);
	}
	
	public static String getProperty(Element element, String propertyName, boolean must) throws XmlException {
		Element propertyElement = findChildElement(element, propertyName);
		String property = propertyElement == null ? null : propertyElement.getTextContent();
		if(must && (property == null || property.length() == 0))
			throw new XmlException(String.format("Unable to find [%s] in [%s]", propertyName, element.getNodeName()));
		return property;
	}
	
	public static void write(Schema schema, String fileName) throws XmlException {
		Document document = toDocument(schema);
		log(String.format("Writing %s schema xml document", schema.getName()));
		fromDocumentToFile(document, new File(fileName));
	}
	
	public static void write(Schema schema, File file) throws XmlException {
		Document document = toDocument(schema);
		log(String.format("Writing %s schema xml document", schema.getName()));
		fromDocumentToFile(document, file);
	}
	
	public static void write(Schema schema, OutputStream out) throws XmlException {
		Document document = toDocument(schema);
		log(String.format("Writing %s schema xml document", schema.getName()));
		fromDocumentToOutputStream(document, out);
	}
	
	public static void write(Schema schema, Writer writer) throws XmlException {
		Document document = toDocument(schema);
		log(String.format("Writing %s schema xml document", schema.getName()));
		fromDocumentToWriter(document, writer);
	}
	
	public static Document toDocument(Schema schema) throws XmlException {
		log(String.format("Creating %s schema xml document", schema.getName()));
		Document document = newDocument();
		Element schemaElement = document.createElement("schema");
		appendProperty(document, schemaElement, "name", schema.getName());
		document.appendChild(schemaElement);
		
		for(Table table : schema.getTables()) {
			Element tableElement = document.createElement("table");
			appendProperty(document, tableElement, "name", table.getName());
			schemaElement.appendChild(tableElement);
			
			for(Column column : table.getColumns()) {
				Element columnElement = document.createElement("column");
				appendProperty(document, columnElement, "name", column.getName());
				appendProperty(document, columnElement, "type", column.getType());
				appendProperty(document, columnElement, "length", column.getLength());
				appendProperty(document, columnElement, "length2", column.getLength2());
				appendProperty(document, columnElement, "primary", column.getPrimary());
				appendProperty(document, columnElement, "not-null", column.getNotNull());
				appendProperty(document, columnElement, "information", column.getInformation());
				tableElement.appendChild(columnElement);
			}
			
			for(Index index : table.getIndexes()) {
				Element indexElement = document.createElement("index");
				appendProperty(document, indexElement, "name", index.getName());
				appendProperty(document, indexElement, "unique", index.getUnique());
				tableElement.appendChild(indexElement);
				
				for(IndexColumn indexColumn : index.getIndexColumns()) {
					Element indexColumnElement = document.createElement("index-column");
					appendProperty(document, indexColumnElement, "column", indexColumn.getColumn().getName());
					appendProperty(document, indexColumnElement, "order", indexColumn.getOrder());
					indexElement.appendChild(indexColumnElement);
				}
			}
			
			for(ForeignKey foreignKey : table.getForeignKeys()) {
				Element foreignKeyElement = document.createElement("foreign-key");
				appendProperty(document, foreignKeyElement, "name", foreignKey.getName());
				appendProperty(document, foreignKeyElement, "referenced-table", foreignKey.getReferencedTable().getName());
				tableElement.appendChild(foreignKeyElement);
				
				for(ForeignKeyColumn foreignKeyColumn : foreignKey.getForeignKeyColumns()) {
					Element foreignKeyColumnElement = document.createElement("foreign-key-column");
					appendProperty(document, foreignKeyColumnElement, "column", foreignKeyColumn.getColumn().getName());
					appendProperty(document, foreignKeyColumnElement, "referenced-column", foreignKeyColumn.getReferencedColumn().getName());
					foreignKeyElement.appendChild(foreignKeyColumnElement);
				}
			}
			
			int rowSize = 10 * table.getColumnCount();
			for(List<Object> row : table.getRows()) {
				StringBuilder builder = toRowString(table.getColumns(), row, rowSize);
				appendProperty(document, tableElement, "row", builder);
				if(rowSize < builder.capacity())
					rowSize = builder.capacity();
			}
		}
		
		
		return document;
	}
	
	public static void appendProperty(Document document, Element parentElement, String propertyName, Object propertyValue) {
		if(propertyValue == null)
			return;
		Element propertyElement = document.createElement(propertyName);
		propertyElement.setTextContent(String.valueOf(propertyValue));
		parentElement.appendChild(propertyElement);
	}
	
	public static StringBuilder toRowString(List<Column> columns, List<Object> row, int rowSize) {
		StringBuilder builder = new StringBuilder(rowSize);
		for(int colIndex=0; colIndex<columns.size(); ++colIndex) {
			Column column = columns.get(colIndex);
			Object cell = row.get(colIndex);
			if(builder.length() > 0)
				builder.append("||");
			if(cell == null)
				builder.append("null");
			else
				builder.append(column.getType().toString(cell));
		}
		return builder;
	}
	
	public static List<Object> fromRowString(List<Column> columns, String rowString) {
		if(rowString == null)
			return null;
		String[] cellStrings = rowString.split("\\|\\|");
		if(columns.size() != cellStrings.length)
			return null;
		ArrayList<Object> row = new ArrayList<>(columns.size());
		for(int colIndex=0; colIndex<columns.size(); ++colIndex) {
			Column column = columns.get(colIndex);
			String cellString = cellStrings[colIndex];
			if("null".equals(cellString))
				row.add(null);
			else {
				try {
					row.add(column.getType().fromString(cellString));
				} catch (Exception e) {
					row.add(null);
				}
			}
		}
		return row;
	}

	public static boolean LogEnabled = false;
	static void log(String text) {
		if(LogEnabled)
			System.out.println(text);
			//Logger.info(text);
	}
}
