package com.vasnabilisim.dbtools.schema.document;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Menderes Fatih G�VEN
 */
public class Schema {

	String name;
	ArrayList<Table> tables = new ArrayList<>();
	
	public Schema() {
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Table> getTables() {
		return tables;
	}

	public Table getTable(String name) {
		for(Table table : tables)
			if(name.equals(table.getName()))
				return table;
		return null;
	}
	
	public void addTable(Table table) {
		table.setSchema(this);
		tables.add(table);
	}
}
