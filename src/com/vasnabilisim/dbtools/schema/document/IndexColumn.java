package com.vasnabilisim.dbtools.schema.document;


/**
 * @author Menderes Fatih G�VEN
 */
public class IndexColumn {

	Index index;
	Column column;
	IndexColumnOrder order;
	
	public IndexColumn() {
	}
	
	@Override
	public String toString() {
		return column.getName() + "-" + order;
	}
	
	public Index getIndex() {
		return index;
	}
	
	public void setIndex(Index index) {
		this.index = index;
	}
	
	public Column getColumn() {
		return column;
	}
	
	public void setColumn(Column column) {
		this.column = column;
	}

	public IndexColumnOrder getOrder() {
		return order;
	}
	
	public void setOrder(IndexColumnOrder order) {
		this.order = order;
	}
}
