package com.vasnabilisim.dbtools.schema;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.vasnabilisim.argument.ArgInfos;
import com.vasnabilisim.dbtools.schema.document.Column;
import com.vasnabilisim.dbtools.schema.document.ColumnType;
import com.vasnabilisim.dbtools.schema.document.ForeignKey;
import com.vasnabilisim.dbtools.schema.document.ForeignKeyColumn;
import com.vasnabilisim.dbtools.schema.document.Index;
import com.vasnabilisim.dbtools.schema.document.IndexColumn;
import com.vasnabilisim.dbtools.schema.document.IndexColumnOrder;
import com.vasnabilisim.dbtools.schema.document.Schema;
import com.vasnabilisim.dbtools.schema.document.SchemaIO;
import com.vasnabilisim.dbtools.schema.document.Table;
import com.vasnabilisim.jdbc.DataSourceInfo;
import com.vasnabilisim.jdbc.DriverInfos;
import com.vasnabilisim.util.ClassLoader;

/**
 * @author Menderes Fatih G�VEN
 */
public class DbReader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			init();
			readDb(ArgInfos.loadArgInfos("config/DbReaderArgs.xml").parse(args));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private static void readDb(Map<String, Object> argumentMap) throws Exception {
		DataSourceInfo dataSourceInfo = DriverInfos.getInstance().getDataSourceInfo((String)argumentMap.get("-driver"), (String)argumentMap.get("-db"));
		Connection connection = dataSourceInfo.createConnection();
		DatabaseMetaData metaData = connection.getMetaData();
		Schema schema = new Schema();
		schema.setName(connection.getCatalog());
		ResultSet tableResultSet = metaData.getTables(null, null, null, new String[] {"TABLE"});
		while(tableResultSet.next()) {
			String tableName = tableResultSet.getString("TABLE_NAME");
			Table table = new Table();
			table.setName(tableName);
			schema.addTable(table);
		}
		tableResultSet.close();
		for(Table table : schema.getTables()) {
			Set<String> primaryColumns = new TreeSet<>();
			ResultSet primaryKeyResultSet = metaData.getPrimaryKeys(null, null, table.getName());
			while(primaryKeyResultSet.next()) {
				primaryColumns.add(primaryKeyResultSet.getString("COLUMN_NAME"));
			}
			primaryKeyResultSet.close();
			
			ResultSet columnResultSet = metaData.getColumns(null, null, table.getName(), null);
			while(columnResultSet.next()) {
				String columnName = columnResultSet.getString("COLUMN_NAME");
				ColumnType columnType = ColumnType.fromSqlType(columnResultSet.getInt("DATA_TYPE"));
				Boolean columnNotNull = columnResultSet.getInt("NULLABLE") == 0;
				Integer columnLength = columnResultSet.getInt("COLUMN_SIZE");
				Integer columnLength2 = columnResultSet.getInt("DECIMAL_DIGITS");
				Column column = new Column();
				column.setName(columnName);
				column.setType(columnType);
				column.setLength(columnLength);
				column.setLength2(columnLength2);
				column.setPrimary(primaryColumns.contains(columnName));
				column.setNotNull(columnNotNull);
				table.addColumn(column);
			}
			columnResultSet.close();
			
			ResultSet indexResultSet = metaData.getIndexInfo(null, null, table.getName(), false, true);
			Index index = new Index();
			while(indexResultSet.next()) {
				String indexName = indexResultSet.getString("INDEX_NAME");
				if(!indexName.equals(index.getName())) {
					index = new Index();
					index.setName(indexName);
					index.setUnique(!indexResultSet.getBoolean("NON_UNIQUE"));
					table.addIndex(index);
				}
				IndexColumn indexColumn = new IndexColumn();
				indexColumn.setColumn(table.getColumn(indexResultSet.getString("COLUMN_NAME")));
				indexColumn.setOrder("A".equals(indexResultSet.getString("ASC_OR_DESC")) ? IndexColumnOrder.asc : IndexColumnOrder.desc);
				index.addIndexColumn(indexColumn);
			}
			indexResultSet.close();
		}

		for(Table table : schema.getTables()) {
			ResultSet foreignKeyResultSet = metaData.getImportedKeys(null, null, table.getName());
			ForeignKey foreignKey = new ForeignKey();
			while(foreignKeyResultSet.next()) {
				String foreignKeyName = foreignKeyResultSet.getString("FK_NAME");
				if(!foreignKeyName.equals(foreignKey.getName())) {
					foreignKey = new ForeignKey();
					foreignKey.setName(foreignKeyName);
					Table referencedTable = schema.getTable(foreignKeyResultSet.getString("PKTABLE_NAME"));
					foreignKey.setReferencedTable(referencedTable);
					table.addForeignKey(foreignKey);
					referencedTable.addReferencedForeignKey(foreignKey);
				}
				ForeignKeyColumn foreignKeyColumn = new ForeignKeyColumn();
				String fkColumnName = foreignKeyResultSet.getString("FKCOLUMN_NAME");
				String pkColumnName = foreignKeyResultSet.getString("PKCOLUMN_NAME");
				foreignKeyColumn.setColumn(table.getColumn(fkColumnName));
				foreignKeyColumn.setReferencedColumn(foreignKey.getReferencedTable().getColumn(pkColumnName));
				
				foreignKey.addForeignKeyColumn(foreignKeyColumn);
			}
			foreignKeyResultSet.close();
		}
		
		connection.close();
		
		SchemaIO.write(schema, (String)argumentMap.get("-out"));
	}
	
	private static void init() throws Exception {
		ClassLoader.init("lib");
		DriverInfos.load("config/DataSource.xml");
	}
}
