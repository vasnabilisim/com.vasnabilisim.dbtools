package com.vasnabilisim.dbtools.schema;

import java.io.File;
import java.util.Map;

import com.vasnabilisim.argument.ArgInfos;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.dbtools.schema.document.Schema;
import com.vasnabilisim.dbtools.schema.document.SchemaIO;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.function.Function;
import com.vasnabilisim.katip.document.function.MySql_ColumnType;
import com.vasnabilisim.katip.generator.Generator;
import com.vasnabilisim.xml.XmlException;

/**
 * @author Menderes Fatih G�VEN
 */
public class SourceGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			init();
			generate(ArgInfos.loadArgInfos("config/SourceGeneratorArgs.xml").parse(args));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	private static void generate(Map<String, Object> params) throws XmlException, KatipException {
		String schemaFileName = (String) params.get("-schema");
		String templateFileName = (String) params.get("-template");
		String targetFolderName = (String) params.get("-target");
	
		Schema schema = SchemaIO.read(schemaFileName);
		Variables variables = new Variables();
		variables.put("schema", schema);
		File templateFile = new File(templateFileName);
		Generator.generate(templateFile.getParent(), templateFile.getName(), targetFolderName, variables);
	}

	private static void init() throws Exception {
		Logger.init("com.vasnabilisim.dbtools", "log");
		Function.register(new MySql_ColumnType());
	}
}
